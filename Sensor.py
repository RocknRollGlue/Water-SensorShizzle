import RPi.GPIO as GPIO 
import time

pin3 = 5 #default level 3 pin
pin2 = 6 #default level 2 pin
pin1 = 13 #default level 1 pin

pinSet = set()
activePinSet = set()
pin_debug = 27 #Debug pin should always be this. DONT MESS WITH DRAGONS
_pin_red=20
_pin_green=21
_pin_blue=16
buzzer = 12

def init():
    setupPins_Defaults()
    
    
def setupPins_Defaults():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin3, GPIO.IN)
    GPIO.setup(pin2, GPIO.IN)
    GPIO.setup(pin1, GPIO.IN)
    pinSet.add(pin1)
    pinSet.add(pin2)
    pinSet.add(pin3)
    GPIO.setup(pin_debug, GPIO.OUT)  
    GPIO.setup(_pin_red, GPIO.OUT)
    GPIO.setup(_pin_green, GPIO.OUT)
    GPIO.setup(_pin_blue, GPIO.OUT)
    GPIO.setup(buzzer, GPIO.OUT)
    

def getLevel():
    activePinSet = getPins()
    counter = 0
    i = 0
   # print('%s active' %(len(activePinSet)))
   # print('%s total'%(len(pinSet)))
    time.sleep(0.1)
    for index in activePinSet:
        if(GPIO.input(index)):
            counter += 1
           # print ('Index added = %s'%(index))
           # print('counter = %s'%(counter))
    while counter-i>=0:

        if (counter ==0):
            return 0
        elif(counter == (len(activePinSet)-i)):
            #print ('Waterlevel is at = %s'%((i+1)))
            return i+1
        i+=1
    return 0
    
    
def getPins():
    GPIO.output(pin_debug, True)
    time.sleep(0.2)
    activePinSet = set()
    for index in pinSet:
        if GPIO.input(index):
            activePinSet.add(index)
            #print ('active pin %s added' %(index))
    
    GPIO.output(pin_debug, False)
    return activePinSet

def buzzerSound(doSound):
    GPIO.output(buzzer, doSound)
    

def setRGB(red, green, blue):
    GPIO.output(_pin_red,red)
    GPIO.output(_pin_green,green)
    GPIO.output(_pin_blue,blue)
    
#init()            
#getPins()

'''
setRGB(True,True,False)
buzzerSound(True)
time.sleep(1)
buzzerSound(False)

while True:
    print GPIO.input(pin1)
    print GPIO.input(pin2)
    print GPIO.input(pin3)
    time.sleep (0.5)
'''    

