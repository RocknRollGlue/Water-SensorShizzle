#import Sensor as sensor
import Email as email
import Sensor as s
import threading
import time
import logging

logging.basicConfig(filename = 'issueLog.log', level=logging.DEBUG,
                    format='%(asctime)s [%(levelname)s] (%(threadName)-10s) %(message)s', 
                    datefmt='%m-%d %H:%M'
                    )

_waterThread = ''
_buzzerThread = ''

mailSent = True

## def will work as main entry point
def onBoot():
    print("Attempting to start")
    try:
        s.init()
        sendBootMail()
	print ('Starting water thread')
        _waterThread = threading.Thread(name='WaterMeasurement', target=startMeasurement).start()
        print ('Setting RGB')
        s.setRGB(False,False,True) #setting the default boot color
        print('started')
    except:
        print("Error creating boot threads") 

#Deprecated
def signalDebug(secondInterval):
    pins = s.getPins()
    logging.debug('%s pins connected' %(len(s.activePinSet))) 
    time.sleep(secondInterval)

def startMeasurement():

    mailSent=False
    while(True):
        time.sleep(2)
        level = s.getLevel()
        print('Waterlevel: %s'%(level))

        if(level == 1):
            runAlarm()
            logging.debug('water level: %s' % (level))
            email.email_text = 'Water is at critical level!'
            if(not mailSent ):
                email.sendEmail()
                mailSent = True
        elif(level <= -1):
            email.email_text = 'issue has been detected. Manual diagnostics required'
            s.setRGB(True,False,False)
        else:
            print('Everything is normal. Water Level: %s'%(level))
            mailSent = False
def runAlarm():
    _buzzerThread = threading.Thread(name='buzzer', target=alarm).start()

def alarm():
    logging.debug('Alarm started')
    while(s.getLevel() == 1):
        s.buzzerSound(True)
        time.sleep(0.2)
        s.buzzerSound(False)

    logging.debug('Alarm Ended')
    
def sendBootMail():
    email.mail_reciever = 'rocknrollglue@gmail.com'
    email.mail_sender_mail = 'waterleveleasv@gmail.com'
    email.mail_sender_password = 'Tdcit123'
    email.subject = "Boot was successfull"
    email.email_text = "Boot was successfull and the water sensor is now functional. IP is %s" % (email.get_ip_address('wlan0'))
    email.body = "Water Sensor V1"
    email.sendEmail()

onBoot()
